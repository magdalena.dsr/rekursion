package bintree;

public class BinNode {
    private Informatiker content;
    private BinNode leftTree;
    private BinNode rightTree;

    public BinNode(Informatiker content, BinNode leftTree, BinNode rightTree) {
        if (content == null) {
            throw new IllegalArgumentException("content ist null.");
        }
        this.content = content;
        this.leftTree = leftTree;
        this.rightTree = rightTree;
    }

    public Informatiker getContent() {
        return content;
    }

    public void setContent(Informatiker content) {
        if (content == null) {
            throw new IllegalArgumentException("content ist null.");
        }
        this.content = content;
    }

    public BinNode getLeftTree() {
        return leftTree;
    }

    public void setLeftTree(BinNode leftTree) {
        this.leftTree = leftTree;
    }

    public BinNode getRightTree() {
        return rightTree;
    }

    public void setRightTree(BinNode rightTree) {
        this.rightTree = rightTree;
    }

}
