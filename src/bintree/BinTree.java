package bintree;

public class BinTree {

    private BinNode root;

    public BinTree(BinNode root) {
        this.root = root;
    }

    public BinTree() {
    }

    public BinNode getRoot() {
        return root;
    }

    public void setRoot(BinNode root) {
        this.root = root;
    }


    public boolean isEmpty() {
        return root == null;
    }

    public Informatiker search(String pName, BinNode subTree) {
        if (subTree == null) {
            return null;
        }


        if (subTree.getContent().getName().equals(pName)) {
            return subTree.getContent();
        }
        if (subTree.getContent().getName().compareTo(pName) < 0) {
            return search(pName, subTree.getRightTree());
        } else {
            return search(pName, subTree.getLeftTree());
        }
    }

    public void insert(Informatiker informatiker, BinNode subTree) {
        if (subTree == null) {
            root = new BinNode(informatiker, null, null);
        }
        if (subTree.getContent().getName().compareTo(informatiker.getName()) == 0) {
            throw new RuntimeException("DU HAST VERSAGT");
        }
        if (subTree.getContent().getName().compareTo(informatiker.getName()) < 0) {
            if (subTree.getRightTree() == null) {
                subTree.setRightTree(new BinNode(informatiker, null, null));
            } else {
                insert(informatiker, subTree.getRightTree());
            }
        } else {
            if (subTree.getLeftTree() == null) {
                subTree.setLeftTree(new BinNode(informatiker, null, null));
            } else {
                insert(informatiker, subTree.getLeftTree());
            }
        }

    }

    public int anzahl(BinNode binNode) {
        if (binNode == null) {
            return 0;
        }
        return 1 + anzahl(binNode.getLeftTree()) + anzahl(binNode.getRightTree());
    }

    public int hoehe() {
        return hoehe(root) - 1;
    }

    private int hoehe(BinNode binNode) {
        if (binNode == null) {
            return 0;
        }
        return 1 + Math.max(hoehe(binNode.getLeftTree()), hoehe(binNode.getRightTree()));
    }

    public static void main(String[] args) {
        BinNode binNode2 = new BinNode(new Informatiker("regen", 56), null, null);
        BinNode binNode1 = new BinNode(new Informatiker("franz", 23), null, null);
        BinNode binNode = new BinNode(new Informatiker("hallo", 31), binNode1, binNode2);
        BinTree binTree = new BinTree(binNode);
        System.out.println(binTree.hoehe());
    }


}
