public class Fakultaet {

    public int fac(final int n) {
        if (n == 1) {
            return 1;
        }
        else {
            return n * fac(n-1);
        }
    }


    public static void main(String[] args) {
        System.out.println(new Fakultaet().fac(5));
    }
}
